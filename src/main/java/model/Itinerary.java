package model;

/**
 * Representa una itinerari.
 */
public class Itinerary {
    private int duration;
    private Leg[] legs;

    public int getDuration() {
        return duration;
    }

    public Leg[] getLegs() {
        return legs;
    }
}
