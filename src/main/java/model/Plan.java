package model;

/**
 * Representa el plan amb les itineraries.
 */
public class Plan {
    private Itinerary[] itineraries;

    public Itinerary[] getItineraries() {
        return itineraries;
    }
}
