package model;

import com.google.gson.annotations.SerializedName;

public class PropertiesEstacio {
    @SerializedName("CODI_GRUP_ESTACIO")
    private int codiGrup;
    @SerializedName("NOM_ESTACIO")
    private String nomEstacio;
    @SerializedName("NOM_LINIA")
    private String nomLinia;
    @SerializedName("DATA_INAUGURACIO")
    private String dataInauguracio;

    // Getter Methods
    public int getCodiGrup() {
        return codiGrup;
    }
    public String getNomEstacio() {
        return nomEstacio;
    }
    public String getNomLinia() {
        return nomLinia;
    }
    public String getDataInauguracio() {
        return dataInauguracio;
    }

}
