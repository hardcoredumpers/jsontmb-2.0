package model;

/**
 * Representa un monument.
 */
public class Monument extends Location {
    public Monument(Location location) {
        super(location);
    }
}
