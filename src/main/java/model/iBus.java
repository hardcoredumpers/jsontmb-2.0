package model;

import com.google.gson.annotations.SerializedName;

public class iBus {
    private String line;
    @SerializedName("text-ca")
    private String textCa;
    private String destination;

    public String getLine() {
        return line;
    }

    public void setLine(String line) {
        this.line = line;
    }

    public String getTextCa() {
        return textCa;
    }

    public void setTextCa(String textCa) {
        this.textCa = textCa;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }
}
