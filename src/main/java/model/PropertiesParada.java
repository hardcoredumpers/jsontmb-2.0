package model;

import com.google.gson.annotations.SerializedName;

public class PropertiesParada {
    @SerializedName("CODI_PARADA")
    private int codiParada;
    @SerializedName("NOM_PARADA")
    private String nomParada;

    public int getCodiParada() {
        return codiParada;
    }

    public String getNomParada() {
        return nomParada;
    }

    public void setCodiParada(int codiParada) {
        this.codiParada = codiParada;
    }

    public void setNomParada(String nomParada) {
        this.nomParada = nomParada;
    }
}
