package model;

import java.util.ArrayList;

/**
 * Representa l'usuari del programa.
 */
public class User {
    private String username;
    private String email;
    private int born;
    private ArrayList<Location> locations;
    private ArrayList<Planner> rutes;
    private ArrayList<String> estacionsInaugurades;
    private ArrayList<LocationPreferida> locationsPreferides;
    private ArrayList<Location> locationsBuscades;

    public User(String username, String email, int born) {
        this.username = username;
        this.email = email;
        this.born = born;
        locations = new ArrayList<Location>();
        rutes = new ArrayList<Planner>();
        estacionsInaugurades = new ArrayList<String>();
        locationsPreferides = new ArrayList<LocationPreferida>();
        locationsBuscades = new ArrayList<>();
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getBorn() {
        return born;
    }

    public void setBorn(int born) {
        this.born = born;
    }

    public ArrayList<Location> getLocations() {
        return locations;
    }

    public void addLocation (Location location) {
        locations.add(location);
    }

    public void showLocations () {
        for (int i = 0; i < locations.size(); i++) {
            System.out.println("-" + locations.get(i).getName());
        }
    }

    public void addRutes(Planner ruta) {
        rutes.add(ruta);
    }

    public ArrayList<Planner> getRutes() {
        return rutes;
    }

    public ArrayList<String> getEstacionsInaugurades() {
        return estacionsInaugurades;
    }

    public void setEstacionsInaugurades(ArrayList<String> estacionsInaugurades) {
        this.estacionsInaugurades = estacionsInaugurades;
    }

    public ArrayList<LocationPreferida> getLocationsPreferides() {
        return locationsPreferides;
    }

    public void setLocationsPreferides(ArrayList<LocationPreferida> locationsPreferides) {
        this.locationsPreferides = locationsPreferides;
    }

    public void addLocationPreferida (LocationPreferida location) {
        locationsPreferides.add(location);
    }

    public void showEstacionsInaugurades() {
        System.out.println("Estacions inaugurades el " + getBorn());
        for (int i = 0; i < estacionsInaugurades.size(); i++) {
            System.out.println("\t-" + estacionsInaugurades.get(i));
        }
    }

    public void setLocations(ArrayList<Location> locations) {
        this.locations = locations;
    }

    public void setRutes(ArrayList<Planner> rutes) {
        this.rutes = rutes;
    }

    public ArrayList<Location> getLocationsBuscades() {
        return locationsBuscades;
    }

    public void setLocationsBuscades(ArrayList<Location> locationsBuscades) {
        this.locationsBuscades = locationsBuscades;
    }

    public void addToHistorial(Location location) {
        locationsBuscades.add(location);
    }
}
