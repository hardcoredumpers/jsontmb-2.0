package model;

/**
 * Representa una parada de bus.
 */
public class FeatureParada extends ParadaGeneral {
    private GeometryParada geometry;
    private PropertiesParada properties;

    public FeatureParada(FeatureParada feature) {
        this.geometry = feature.geometry;
        this.properties = feature.properties;
    }

    public GeometryParada getGeometry() {
        return geometry;
    }

    public void setGeometry(GeometryParada geometry) {
        this.geometry = geometry;
    }

    public PropertiesParada getProperties() {
        return properties;
    }

    public void setProperties(PropertiesParada properties) {
        this.properties = properties;
    }
}
