package model;

public class Planner {
    private String fromPlace;
    private String toPlace;
    private String date;
    private String time;
    private String arriveBy;
    private String mode;
    private String maxWalkDistance;
    private PlanResponse plan;

    public Planner(String fromPlace, String toPlace, String date, String time, String arriveBy, String mode, String maxWalkDistance) {
        this.fromPlace = fromPlace;
        this.toPlace = toPlace;
        this.date = date;
        this.time = time;
        this.arriveBy = arriveBy;
        this.mode = mode;
        this.maxWalkDistance = maxWalkDistance;
    }

    public Planner(String fromPlace, String toPlace, String date, String time, String arriveBy, String mode, String maxWalkDistance, PlanResponse plan) {
        this.fromPlace = fromPlace;
        this.toPlace = toPlace;
        this.date = date;
        this.time = time;
        this.arriveBy = arriveBy;
        this.mode = mode;
        this.maxWalkDistance = maxWalkDistance;
        this.plan = plan;
    }
    public String getFromPlace() {
        return fromPlace;
    }

    public String getToPlace() {
        return toPlace;
    }

    public String getDate() {
        return date;
    }

    public String getTime() {
        return time;
    }

    public String getArriveBy() {
        return arriveBy;
    }

    public String getMode() {
        return mode;
    }

    public String getMaxWalkDistance() {
        return maxWalkDistance;
    }

    public PlanResponse getPlan() {
        return plan;
    }
}
