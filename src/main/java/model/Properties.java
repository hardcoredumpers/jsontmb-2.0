package model;

import com.google.gson.annotations.SerializedName;

public class Properties {
    @SerializedName("CODI_LINIA")
    private String codiLinia;
    @SerializedName("NOM_LINIA")
    private String nomLinia;

    public String getCodiLinia() {
        return codiLinia;
    }

    public void setCodiLinia(String codiLinia) {
        this.codiLinia = codiLinia;
    }

    public void setNomLinia(String nomLinia) {
        this.nomLinia = nomLinia;
    }

    public String getNomLinia() {
        return nomLinia;
    }
}