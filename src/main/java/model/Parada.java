package model;

/**
 * Representa una agrupació de parades de busos.
 */
public class Parada {
    private int totalFeatures;
    private FeatureParada[] features;

    public int getTotalFeatures() {
        return totalFeatures;
    }

    public void setTotalFeatures(int totalFeatures) {
        this.totalFeatures = totalFeatures;
    }

    public FeatureParada[] getFeatures() {
        return features;
    }

    public void setFeatures(FeatureParada[] features) {
        this.features = features;
    }
}
