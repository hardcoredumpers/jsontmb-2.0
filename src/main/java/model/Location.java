package model;

/**
 * Representa una localització.
 */
public class Location {
    private String name;
    private double[] coordinates;
    private String description;
    private String architect;
    private int inauguration;
    private String[] characteristics;
    private int stars;

    public Location(String name, double[] coordinates, String description) {
        this.name = name;
        this.coordinates = coordinates;
        this.description = description;
    }

    public Location(Location location) {
        this.name = location.name;
        this.coordinates = location.coordinates;
        this.description = location.description;
        this.architect = location.architect;
        this.characteristics = location.characteristics;
        this.inauguration = location.inauguration;
        this.stars = location.stars;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double[] getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(double[] coordinates) {
        this.coordinates = coordinates;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getArchitect() {
        return architect;
    }

    public void setArchitect(String architect) {
        this.architect = architect;
    }

    public String[] getCharacteristics() {
        return characteristics;
    }

    public void setCharacteristics(String[] characteristics) {
        this.characteristics = characteristics;
    }

    public int getStars() {
        return stars;
    }

    public void setStars(int stars) {
        this.stars = stars;
    }

    public int getInauguration() {
        return inauguration;
    }

    public void setInauguration(int inauguration) {
        this.inauguration = inauguration;
    }
}
