package model;

/**
 * Representa la resposta que ve de la API.
 */
public class PlanResponse {
    Plan plan;

    public Plan getPlan() {
        return plan;
    }
}
