package model;

/**
 * Representa una estació de metro.
 */
public class FeatureEstacio extends ParadaGeneral{
    private GeometryParada geometry;
    private PropertiesEstacio properties;

    public FeatureEstacio(FeatureEstacio feature) {
        this.geometry = feature.geometry;
        this.properties = feature.properties;
    }

    public GeometryParada getGeometry() {
        return geometry;
    }

    public void setGeometry(GeometryParada geometry) {
        this.geometry = geometry;
    }

    public PropertiesEstacio getProperties() {
        return properties;
    }

    public void setProperties(PropertiesEstacio properties) {
        this.properties = properties;
    }
}
