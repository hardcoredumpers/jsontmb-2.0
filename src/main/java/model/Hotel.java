package model;

/**
 * Representa un hotel.
 */
public class Hotel extends Location {
    public Hotel(Location location) {
        super(location);
    }
}
