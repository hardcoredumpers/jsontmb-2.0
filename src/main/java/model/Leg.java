package model;

/**
 * Representa una ruta.
 */
public class Leg {
    private String mode;
    private String route;
    private int duration;
    private Stop from;
    private Stop to;
    private Stop[] intermediateStops;

    public String getMode() {
        return mode;
    }

    public String getRoute() {
        return route;
    }

    public int getDuration() {
        return duration;
    }

    public Stop getFrom() {
        return from;
    }

    public Stop getTo() {
        return to;
    }

    public Stop[] getStops() {
        return intermediateStops;
    }
}


