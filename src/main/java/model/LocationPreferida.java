package model;

import java.util.Date;

/**
 * Representa una localització preferida.
 */
public class LocationPreferida {
    private Location location;
    private String tipus;
    private Date fechaAssignacio;

    public LocationPreferida(Location location, String tipus, Date fechaAssignacio) {
        this.location = location;
        this.tipus = tipus;
        this.fechaAssignacio = fechaAssignacio;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public String getTipus() {
        return tipus;
    }

    public void setTipus(String tipus) {
        this.tipus = tipus;
    }

    public Date getFechaAssignacio() {
        return fechaAssignacio;
    }

    public void setFechaAssignacio(Date fechaAssignacio) {
        this.fechaAssignacio = fechaAssignacio;
    }
}
