package model;

/**
 * Representa una agrupació d'estacions de metro.
 */
public class Estacio {
    private int totalFeatures;
    private FeatureEstacio[] features;


    public int getTotalFeatures() {
        return totalFeatures;
    }

    public void setTotalFeatures(int totalFeatures) {
        this.totalFeatures = totalFeatures;
    }

    public FeatureEstacio[] getFeatures() {
        return features;
    }

    public void setFeatures(FeatureEstacio[] features) {
        this.features = features;
    }
}
