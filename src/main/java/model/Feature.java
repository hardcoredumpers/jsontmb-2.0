package model;

public class Feature {
    private Geometry geometry;
    private Properties properties;

    public Feature(Feature feature) {
        this.geometry = feature.geometry;
        this.properties = feature.properties;
    }

    public Geometry getGeometry() {
        return geometry;
    }

    public void setGeometry(Geometry geometry) {
        this.geometry = geometry;
    }

    public Properties getProperties() {
        return properties;
    }

    public void setProperties(Properties properties) {
        this.properties = properties;
    }
}
