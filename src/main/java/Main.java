import model.User;
import utils.Menu;
import utils.ResultManager;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Menu m = new Menu();

        Scanner sc = new Scanner(System.in);
        m.showWelcomeMessage();
        System.out.println("Nom d'usuari:");
        String username = sc.next();
        System.out.println("Correu electrònic:");
        String email = sc.next();
        System.out.println("Any de naixement:");
        int born = sc.nextInt();

        ResultManager resultManager = new ResultManager(new User(username,email,born));
        System.out.println("\nLa informació s'ha registrat amb èxit!");
        resultManager.readJSON();
        do {
            do {
                m.menuPrincipal();
                m.optionInput();
            } while (!m.validatePrincipalInput());
            resultManager.start(m.getOptionA());
        } while (!m.exitProgram());
    }
}
