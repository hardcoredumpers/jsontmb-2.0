package utils;

import java.util.Scanner;

/**
 * Representa el Menu del programa amb els missatges d'error i comprovacions de les opcions introduïdes per l'usuari.
 */
public class Menu {
    private static final int MAXPRINCIPAL = 5;
    private static final int MINPRINCIPAL = 1;
    private static final int MAX = 'f';
    private static final int MIN = 'a';
    private String option;
    private Scanner sc;

    public Menu () {
        sc = new Scanner(System.in);
    }

    /**
     * Mostra el missatge de benvinguts del programa.
     */
    public void showWelcomeMessage() {
        System.out.println("Benvingut a l'aplicació de TMBJson! Si us plau, introdueix les dades que se't demanen.");
    }

    /**
     * Agafa l'opció introduïda per l'usuari.
     */
    public void optionInput() {
        option = sc.nextLine();
    }

    /**
     * Agafa l'opció per al menú principal i la converteix a un enter
     * @return l'enter de l'opció introduïda.
     */
    public int getOptionA () {
        return Integer.parseInt(option);
    }

    /**
     * Mostra el menú principal per pantalla.
     */
    public void menuPrincipal() {
        System.out.println("\n1. Gestió d'usuari");
        System.out.println("2. Buscar localitzacions.");
        System.out.println("3. Planejar ruta");
        System.out.println("4. Temps d'espera del bus");
        System.out.println("5. Sortir");
        System.out.println("\n");
        System.out.println("Selecciona una opcio: ");
    }

    /**
     * Mostra el misstage d'error per pantalla segons el cas.
     * @param cas el cas en que volem mostrar el missatge d'error.
     */
    private void showError(int cas) {
        switch (cas) {
            case 1:
                System.out.println("Introdueix una opció vàlida (1-5).");
                break;
            case 2:
                System.out.println("Introdueix una opció vàlida (a-f).");
                break;
        }
    }

    /**
     * Converteix un string a un enter
     * @param str el string que es vol convertir
     * @return l'enter convertit
     */
    private int convertStringToNumber(String str) {
        int i = 0;
        double number = 0;

        // Calcular el valor i convertir l'opció a un enter
        while (str.length() > i && str.charAt(i) >= '0' && str.charAt(i) <= '9') {
            number = number * 10 + (str.charAt(i) - '0');
            i++;
        }
        return (int) number;
    }
    public char getOptionB () {
        return option.toLowerCase().charAt(0);
    }

    /**
     * Comprova si l'opció introduïda per al menú principal és vàlida.
     * @return l'opció introduïda convertit a un enter.
     */
    private int checkOption() {
        StringBuilder stringBuilder = new StringBuilder();
        String ascii = "";
        long numeric;
        int opt = -1;

        for (int i = 0; i < option.length(); i++) {
            stringBuilder.append((int)option.charAt(i));
            char c = option.charAt(i);
        }
        ascii = stringBuilder.toString();
        numeric = convertStringToNumber(ascii);
        if (numeric >= '1' && numeric <= '5') {
            opt = Character.getNumericValue(Integer.parseInt(ascii));
        }
        return opt;
    }

    /**
     * Comprovar si el input del menu principal és vàlid.
     * @return true si està dins dels limits. Altrament, false.
     */
    public boolean validatePrincipalInput () {
        int aux;
        aux = checkOption();
        if (aux >= MINPRINCIPAL && aux <= MAXPRINCIPAL) { return true; }
        showError(1);
        return false;
    }

    /**
     * Mostrar el menú de l'opció "Gestió d'usuari".
     */
    public void menuGestioUsuari() {
        System.out.println("\n");
        System.out.println("a) Les meves localitzacions");
        System.out.println("b) Historial de localitzacions");
        System.out.println("c) Les meves rutes");
        System.out.println("d) Parades i estacions preferides");
        System.out.println("e) Estacions inaugurades el meu any de naixement");
        System.out.println("f) Tornar al menú principal");
        System.out.println("\n");
        System.out.println("Selecciona una opcio: ");
    }

    private int checkCharOption() {
        int opt = -1;
        //Miramos el tamaño de lo que ha introducido
        if (option.length() >  1) {
            return opt;
        }
        int numeric = option.toLowerCase().charAt(0);
        if (numeric >= 'a' && numeric <= 'f') {
            opt = numeric;
        }
        return opt;
    }

    /**
     * Comprova si l'opció introduïda està dins dels limits del menú de la gestió d'usuaris.
     * @return true si és vàlida. Altrament, false.
     */
    public boolean validateInput() {
        int aux = checkCharOption();
        if (aux >= MIN && aux <= MAX) {
            return true;
        }
        showError(2);
        return false;
    }

    /**
     * Comprovar si l'usuari ha escollit sortir del programa.
     * @return true si l'usuari vol sortir del programa. Altrament, false.
     */
    public boolean exitProgram () {
        return Integer.parseInt(option) == MAXPRINCIPAL;
    }

    /**
     * Comprovar si l'usuari ha escollit sortir del menú de gestió d'usuaris.
     * @return true si l'usuari vol sortir del programa. Altrament, false.
     */
    public boolean exit () { return option.toLowerCase().charAt(0) == MAX; }


}
