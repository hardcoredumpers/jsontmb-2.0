package utils;

import com.google.gson.Gson;
import model.*;
import webservices.TMBClient;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import static java.util.stream.Collectors.toMap;

public class ResultManager {
    private static final String PATH = "src/main/resources/localitzacions.json";
    //protected static final String LATITUDE_PATTERN="^(\\+|-)?(?:90(?:(?:\\.0{1,6})?)|(?:[0-9]|[1-8][0-9])(?:(?:\\.[0-9]{1,6})?))$";
    private static final String COORDS_PATTERN = "^(\\-?\\d+(\\.\\d+)?)$";
    private DataModel data;
    private User user;
    private ArrayList<Location> locations;

    public ResultManager(User user) {
        this.user = user;
    }

    /**
     * Llegeix el JSON de les localitzacions per carregar la informació al sistema.
     */
    public void readJSON() {
        data = new DataModel();
        Gson gson = new Gson();
        BufferedReader br = null;

        try {
            br = new BufferedReader(new FileReader(PATH));
            data = gson.fromJson(br, DataModel.class);
            separateByDataType();
            inauguracioEstacions(user.getBorn());
        } catch (FileNotFoundException e) {
            System.out.println("\nError, el fitxer localitzacions.json no existeix o no es troba.");
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * Determinar el tipus de la localització a partir de la seva informació i crea un array dinamic de localitzacions.
     */
    private void separateByDataType() {
        this.locations = new ArrayList<Location>();
        int numLocations = data.getLocations().length;
        Location[] locationsAux = data.getLocations();
        for (int i = 0; i < numLocations; i++) {
            Location location;
            // En cas de que sigui un monument
            if (locationsAux[i].getArchitect() != null) {
                location = new Monument(locationsAux[i]);
            } else if (locationsAux[i].getStars() != 0) { // En cas de que sigui un hotel
                location = new Hotel(locationsAux[i]);
            } else if (locationsAux[i].getCharacteristics() != null) {
                location = new Restaurant(locationsAux[i]);
            } else {
                location = new Location(locationsAux[i]);
            }
            this.locations.add(location);
        }
    }

    /**
     * El switch case que conté totes las funcionalitats segons la opció introduïda.
     * @param optionA l'opció introduïda.
     */
    public void start(int optionA) {
        switch (optionA) {
            case 1:
                Menu m = new Menu();
                do {
                    do {
                        m.menuGestioUsuari();
                        m.optionInput();
                    } while (!m.validateInput());
                    switch (m.getOptionB()) {
                        case 'a':
                            lesMevesLocalitzacions();
                            break;
                        case 'b':
                            mostrarHistorial();
                            break;
                        case 'c':
                            lesMevesRutes();
                            break;
                        case 'd':
                            getParadesProperes();
                            break;
                        case 'e':
                            showEstacionsInaugurades();
                            break;
                    }
                } while (!m.exit());
                break;
            case 2:
                buscarLocalitzacions();
                break;
            case 3:
                planejarRuta();
                break;
            case 4:
                getTempsEsperaBus();
                break;
        }


    }

    /**
     * Comprova que les coordenades són vàlides seguint el sistema de EPSG:4326
     * @param coord latitud i longitud
     * @return un booleà
     */
    private boolean coordenadesValid(String coord, String type) {
        if (coord != null) {
            if (!coord.matches(COORDS_PATTERN)) {
                System.out.println("Error, el format de la coordinada introduïda no és vàlida\n");
                return false;
            }
            switch (type) {
                case "lat":
                    double x = Double.valueOf(coord);
                    if (x >= -90 && x <= 90) {
                        return true;
                    }
                    else {
                        System.out.println("Error, la latitud introduïda no és vàlida [-90,90].\n");
                        return false;
                    }
                case "lon":
                    double y = Double.valueOf(coord);
                    if (y >= -180 && y <= 180) {
                        return true;
                    }
                    else {
                        System.out.println("Error, la longitud introduïda no és vàlida [-180,180].\n");
                        return false;
                    }
            }
        }
        System.out.println("Error, no ha introduït cap valor\n");
        return false;
    }

    /* -------------------------------------- Cas 1a: Les meves localitzacions -------------------------------------- */
    private void lesMevesLocalitzacions() {
        Scanner sc = new Scanner(System.in);
        String in, nom, descripcio = "";
        String lon, lat = "";
        double[] coord = new double[2];
        do {
            if (user.getLocations().size() < 1) {
                System.out.println("No tens cap localització creada.");
            }
            else { user.showLocations(); }
            System.out.println("\nVols crear una nova localització? (sí/no)");
            in = sc.nextLine();
            if (in.toLowerCase().equals("si") || in.toLowerCase().equals("sí")) {
                do {
                    System.out.println("Nom de la localització: ");
                    nom = sc.nextLine();
                } while (existeixLocalitzacio(nom));
                do {
                    System.out.println("Longitud: ");
                    lon = sc.nextLine();
                } while (!coordenadesValid(lon,"lon"));
                do {
                    System.out.println("Latitud: ");
                    lat = sc.nextLine();
                } while (!coordenadesValid(lat,"lat"));
                coord[0] = Double.valueOf(lat);
                coord[1] = Double.valueOf(lon);
                System.out.println("Descripció: ");
                descripcio = sc.nextLine();
                user.addLocation(new Location(nom, coord, descripcio));
                System.out.println("\nLa informació s'ha registrat amb èxit!\n");
            }
        } while (!inputValid(in) || !in.toLowerCase().equals("no"));
    }

    /**
     * Comprova que la localització existeix a la llista de localitzacions de l'usuari o al fitxer .json
     * @param localitzacio nom d'una localització
     * @return un booleà
     */
    private boolean existeixLocalitzacio(String localitzacio) {
        for (int i = 0; i < locations.size(); i++) {
            if (locations.get(i).getName().equals(localitzacio)) {
                System.out.println("Error! Aquesta localització ja existeix.");
                return true;
            }
        }
        if (user.getLocations() != null) {
            for (int i = 0; i < user.getLocations().size(); i++) {
                if (user.getLocations().get(i).getName().equals(localitzacio)) {
                    System.out.println("Error! Aquesta localització ja existeix.");
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Comprova si l'usuari ha introduït un sí o un no.
     * @param input
     * @return un booleà
     */
    private boolean inputValid (String input) {
        String aux = input.toLowerCase();
        if (aux.equals("sí") || aux.equals("si") || aux.equals("no")) {
            return true;
        }
        System.out.println("\nError! S'ha de introduir 'sí' o 'no'");
        return false;
    }


    /* ---------------------------------- Cas 1b: Historial de localitzacions --------------------------------------- */
    /**
     * Mostrar una llista de les localitzacions buscades per l'usuari. El més recent apareixerà al principi
     * de la llista.
     */
    private void mostrarHistorial() {
        if (user.getLocationsBuscades().isEmpty()) {
            System.out.println("Encara no has buscat cap localització!");
            System.out.println("Per buscar-ne una, accedeix a l'opció 2 del menú principal.");
        } else {
            System.out.println("Localitzacions buscades:");
            int len = user.getLocationsBuscades().size();
            for (int i = len - 1; i >= 0; i--) {
                Location location = user.getLocationsBuscades().get(i);
                System.out.println("\t-" + location.getName());
            }
        }
    }

    /* ----------------------------------------- Cas 1c: Les meves rutes -------------------------------------------- */

    /**
     * A partir de la planificació de rutes en l'opció 3, aquesta funció mostra la llista de rutes que ha buscat.
     */
    private void lesMevesRutes () {
        if (user.getRutes().size() < 1) {
            System.out.println("Encara no has realitzat cap ruta:(\nPer buscar-ne una, accedeix a l'opció 3 del menú principal.");
        }
        else {
            ArrayList<Planner> rutes = user.getRutes();
            for (int i = 0; i < rutes.size(); i++) {
                System.out.println("->Ruta " + (i+1) + ":");
                System.out.println("\t-Origen: " + rutes.get(i).getFromPlace());
                System.out.println("\t-Destí: " + rutes.get(i).getToPlace());
                System.out.println("\t-Dia de sortida: " + rutes.get(i).getDate() + " a les " + rutes.get(i).getTime());
                System.out.println("\t-Màxima distància caminant: " + rutes.get(i).getMaxWalkDistance() + " metres");
                showPlanRuta(rutes.get(i).getPlan());
            }
        }
    }

    /* -------------------------- Cas 1d:  Parades i estacions preferides ------------------------------------------- */

    /**
     * Busca totes les parades de bus i estacions de metro que estiguin en un radi de 0.5 km de cada localització
     * preferida
     */
    private void getParadesProperes() {
        TMBClient tmb = new TMBClient();
        Gson gson = new Gson();
        Map<ParadaGeneral, Double> paradesProperes = new HashMap<ParadaGeneral, Double>();

        if (this.user.getLocationsPreferides().isEmpty()) {
            System.out.println("Per tenir parades i estacions preferides es requereix haver creat una " +
                    "localització preferida anteriorment.");
        } else {
            String responseParades = tmb.getParades();
            String responseEstacions = tmb.getEstacions();
            Parada paradesBus = gson.fromJson(responseParades, Parada.class);
            Estacio estacionsMetro = gson.fromJson(responseEstacions, Estacio.class);
            for (LocationPreferida location : this.user.getLocationsPreferides()) {
                // Netegem la llista de parades properes
                paradesProperes.clear();

                // Comprovar quines son les estacions de metro més properes
                for (int i = 0; i < estacionsMetro.getTotalFeatures(); i++) {
                    FeatureEstacio estacio = estacionsMetro.getFeatures()[i];
                    double[] coords = estacio.getGeometry().getCoordinates();
                    double distance = calculateDistance(location.getLocation().getCoordinates(), coords);
                    if (distance <= 0.5) {
                        paradesProperes.put(estacio, distance);
                    }

                }

                // Comprovar quines son les parades de bus més properes
                for (int i = 0; i < paradesBus.getTotalFeatures(); i++) {
                    FeatureParada parada = paradesBus.getFeatures()[i];
                    double[] coords = parada.getGeometry().getCoordinates();
                    double distance = calculateDistance(location.getLocation().getCoordinates(), coords);
                    if (distance <= 0.5) {
                        paradesProperes.put(parada, distance);
                    }

                }

                // Ordenar segons la proximitat a la localitzacio
                Map<ParadaGeneral, Double> sorted = paradesProperes
                        .entrySet()
                        .stream()
                        .sorted(Map.Entry.comparingByValue())
                        .collect(toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e2, LinkedHashMap::new));

                System.out.println("-" + location.getLocation().getName());
                printParadesProperes(sorted);
                System.out.println();
            }
        }
    }

    /**
     * Mostrar totes les parades de bus i estacions de metro que estiguin en un radi de 0.5 km de
     * cada localització preferida
     * @param parades les parades de bus i estacions a prop de la localització
     */
    private void printParadesProperes(Map<ParadaGeneral, Double> parades) {
        if (parades.size() > 0) {
            int i = 1;
            for (Map.Entry<ParadaGeneral, Double> entry : parades.entrySet()) {
                if (entry.getKey() instanceof FeatureParada) {
                    FeatureParada featureParada = (FeatureParada) entry.getKey();
                    System.out.print(i + ") " + featureParada.getProperties().getNomParada() + " (" +
                            featureParada.getProperties().getCodiParada() + ") ");
                    System.out.println("BUS ");
                } else {
                    if (entry.getKey() instanceof FeatureEstacio) {
                        FeatureEstacio featureEstacio = (FeatureEstacio) entry.getKey();
                        System.out.print(i + ") " + featureEstacio.getProperties().getNomEstacio() + " (" +
                                featureEstacio.getProperties().getCodiGrup() + ") ");
                        System.out.println("METRO ");
                    }
                }
                ++i;
            }
        } else {
            System.out.println("TMB està fent tot el possible perquè el bus i el metro arribin fins aquí.");
        }
    }

    /**
     * Calcula la distància a la ubicació mitjançant la fórmula Haversine
     *
     * @param org  la ubicació de origen
     * @param dest la ubicació de destí
     * @return la distància entre les coordenades
     */
    public double calculateDistance(double[] org, double[] dest) {
        final int radiusEarth = 6371; // in miles
        double distance = 0;
        double distLongitud = Math.toRadians(dest[0] - org[0]);
        double distLatitud = Math.toRadians(dest[1] - org[1]);

        double a = Math.pow(Math.sin(distLatitud / 2), 2) + Math.cos(Math.toRadians(org[1]))
                * Math.cos(Math.toRadians(dest[1])) * Math.pow(Math.sin(distLongitud / 2), 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        distance = radiusEarth * c;

        return distance;
    }

    /* -------------------------- Cas 1e:  Estacions inaugurades el meu any de naixement ---------------------------- */
    /**
     * Guarda les estacions inaugurades el mateix any que ha nascut l'usuari a partir de la API de TMB.
     * Es busca una estació a partir del codi de la linia, i per cada línia hi pot haver diferents estacions.
     * @param any any de naixement de l'usuari.
     */
    private void inauguracioEstacions (int any) {
        TMBClient tmb = new TMBClient();
        Gson gson = new Gson();
        Map<String, Integer> map = new HashMap<String, Integer>();

        String jsonLiniaMetro = tmb.getLinies("metro");
        Linia liniaMetro = gson.fromJson(jsonLiniaMetro, Linia.class);
        int totalLinies = liniaMetro.getTotalFeatures();

        for (int i = 0; i < totalLinies; i++) {
            String codi = liniaMetro.getFeatures()[i].getProperties().getCodiLinia();
            String jsonLiniaEstacio = tmb.getLiniesEstacions(codi);
            Estacio estacions = gson.fromJson(jsonLiniaEstacio, Estacio.class);
            int totalEstacions = estacions.getTotalFeatures();
            for (int j = 0; j < totalEstacions; j++) {
                FeatureEstacio featureEstacio = estacions.getFeatures()[j];
                PropertiesEstacio propertiesEstacio = featureEstacio.getProperties();
                if (getAnyInauguracio(propertiesEstacio.getDataInauguracio()) == user.getBorn()) {
                    String str = propertiesEstacio.getNomEstacio() + " " + propertiesEstacio.getNomLinia();
                    map.put(str, Integer.parseInt(codi));
                }
            }
        }
        //Ordenem segons el codi de la linia
        map = map.entrySet().stream().sorted(Collections.reverseOrder(Map.Entry.comparingByValue())).collect(toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e2, LinkedHashMap::new));
        Set<String> keySet = map.keySet();
        //Transformem el hashmap en un arraylist
        ArrayList<String> estacionsInagurades = new ArrayList<String>(keySet);
        user.setEstacionsInaugurades(estacionsInagurades);
    }

    /**
     * Agafa l'any de inauguració de l'estació.
     * @param str l'any de naixement de l'usuari
     * @return retorna l'any de naixement
     */
    private int getAnyInauguracio(String str) {
        String[] coords = str.split("\\-");
        return Integer.parseInt(coords[0]);
    }

    /**
     * Mostra les estacions inaugurades de l'any que ha nascut l'usuari en cas de haver-hi.
     */
    private void showEstacionsInaugurades() {
        if (user.getEstacionsInaugurades().size() >= 1) {
            user.showEstacionsInaugurades();
        } else {
            System.out.println("Cap estació es va inaugurar el teu any de naixement :(");
        }
    }

    /* ------------------------------- Cas 2:  Buscar Localitzacions ------------------------------------------------ */

    /**
     * Mostra tota la infomació d'una localització segons el seu tipus
     */
    public void buscarLocalitzacions() {
        Scanner sc = new Scanner(System.in);
        String nomLocalitzacio;
        String option;
        String tipus;

        // Demanar el nom de la localització que volem buscar
        System.out.println("Introdueix el nom d'una localització:");
        nomLocalitzacio = sc.nextLine();

        // Fer una búsqueda a la llista de localitzacions
        Location existingLocation = getLocalitzacioExistent(nomLocalitzacio);
        if (existingLocation != null) {
            System.out.println("\nPosició: " + existingLocation.getCoordinates()[1] + ", " + existingLocation.getCoordinates()[0]);
            System.out.println("Descripció:");
            System.out.println(existingLocation.getDescription());

            // Mostrem més informació segons el seu tipus
            if (existingLocation instanceof Hotel) {
                System.out.println("Stars: " + existingLocation.getStars());
            }
            if (existingLocation instanceof Restaurant) {
                System.out.println("Característiques:");
                for (int i = 0; i < existingLocation.getCharacteristics().length; i++) {
                    System.out.println("\t" + existingLocation.getCharacteristics()[i]);
                }
            }
            if (existingLocation instanceof Monument) {
                System.out.println("Arquitecte: " + existingLocation.getArchitect());
                System.out.println("Inauguració: " + existingLocation.getInauguration());
            }

            // Guardar al historial
            this.user.addToHistorial(existingLocation);

            // Preguntem si l'usuari vol guardar-la com a preferida
            System.out.println("\nVols guardar la localització trobada com a preferida? (sí/no)");
            option = sc.nextLine();
            if (option.toLowerCase().equals("si") || option.toLowerCase().equals("sí")) {
                do {
                    System.out.println("\nTipus(casa/feina/estudis/oci/cultura): ");
                    tipus = sc.nextLine();
                } while (!tipusLocalitzacioValid(tipus));
                Date date = new Date();
                user.addLocationPreferida(new LocationPreferida(existingLocation, tipus, date));
                System.out.println("\n" + existingLocation.getName() + " s'ha assignat com a una nova localització preferida!\n");
            }
        } else {
            System.out.println("Ho sentim, no hi ha cap localització amb aquest nom.");
        }
    }

    /**
     * Comprova si el tipus introduït és vàlid
     * @param tipus el tipus de la localització
     * @return true si és vàlid. Altrament, false.
     */
    private boolean tipusLocalitzacioValid(String tipus) {
        if (tipus.equals("casa") || tipus.equals("feina") || tipus.equals("estudis") || tipus.equals("oci") || tipus.equals("cultura")) {
            return true;
        } else {
            System.out.println("Error! S'ha d'introduir \"casa\", \"feina\", \"estudis\", \"oci\" o \"cultura\".");
            return false;
        }
    }

    /* ---------------------------------------- Cas 3: Planejar ruta ------------------------------------------------ */

    /**
     * Funcionalitat per planificar la ruta. Demana l'usuari les dades necesàries per l'API i retorna diferents
     * itineraris segons la seva cerca. Agafa el millor itinerari depenent del punt de sortida y la màxima
     * distància a caminar.
     */
    private void planejarRuta() {
        Scanner sc = new Scanner(System.in);
        String org, dest, arriveBy, dia, hora, walkDistance = "";
        String orgCoord = "";
        String orgNomLoc = "";
        String destCoord = "";
        String destNomLoc = "";
        PlanResponse plan = new PlanResponse();
        do {
            System.out.println("Origen? (lat,lon/nom localització)");
            org = sc.nextLine();
            orgCoord = validateOrgDest(org);
            //Si la entrada no era una coordenada, i ens han retornat una, hem de guardar el nom de la localització
            if (!org.contains(",") && orgCoord != null) {
                orgNomLoc = org;
            }
        } while (orgCoord == null);
        do {
            System.out.println("Destí? (lat,lon/nom localització)");
            dest = sc.nextLine();
            destCoord = validateOrgDest(dest);
            if (!dest.contains(",") && destCoord != null) {
                destNomLoc = dest;
            }
        } while (destCoord == null);
        do {
            System.out.println("Dia/hora seran de sortida o d'arribada? (s/a)");
            arriveBy = sc.nextLine().toLowerCase();
            arriveBy = validateSortidaArribada(arriveBy);
        } while (arriveBy == null);
        do {
            System.out.println("Día? (MM-DD-YYYY)");
            dia = sc.nextLine();
        } while (!validarData(dia));
        System.out.println("Hora? (HH:MMam/HH:MMpm)");
        hora = sc.nextLine();
        System.out.println("Màxima distància caminant en metres?");
        walkDistance = sc.nextLine();


        TMBClient tmb = new TMBClient();
        Gson gson = new Gson();
        String responseRuta = tmb.planRuta(new Planner(orgCoord, destCoord,dia,hora,arriveBy,"TRANSIT,WALK",walkDistance));
        if (!responseRuta.equals("")) {
            plan = gson.fromJson(responseRuta,PlanResponse.class);
            showPlanRuta(plan);

            //Afegim la ruta a la llista de l'usuari
            if (!org.contains(",")) {
                user.addRutes(new Planner(orgNomLoc, destCoord, dia, hora, arriveBy, "TRANSIT,WALK", walkDistance, plan));
            }
            if (!dest.contains(",")) {
                user.addRutes(new Planner(orgCoord, destNomLoc, dia, hora, arriveBy, "TRANSIT,WALK", walkDistance, plan));
            }
            if (!org.contains(",") && !dest.contains(",")) {
                user.addRutes(new Planner(orgNomLoc, destNomLoc, dia, hora, arriveBy, "TRANSIT,WALK", walkDistance, plan));
            }
            if (org.contains(",") && dest.contains(",")) {
                user.addRutes(new Planner(orgCoord, destCoord, dia, hora, arriveBy, "TRANSIT,WALK", walkDistance, plan));
            }
        }
    }

    /**
     * Busca el nom de la localització si existeix per agafar les seves coordenades, i en cas contrari,
     * mostrará un missatge d'error.
     * @param str és un nom d'una localització
     */
    public Location getLocalitzacioExistent (String str) {
        //Mirem a la llista de localitzacions del fitxer .json
        for (int i = 0; i < locations.size(); i++) {
            if (locations.get(i).getName().equals(str)){
                return locations.get(i);
            }
        }
        //Mirem a la llista de localitzacions de l'usuari
        if (user.getLocations() != null) {
            for (int i = 0; i < user.getLocations().size(); i++) {
                if (user.getLocations().get(i).getName().equals(str)) {
                    return user.getLocations().get(i);
                }
            }
        }
        System.out.println("Ho sentim, aquesta localització no és vàlida :(\n");
        return null;
    }

    /**
     * Analitza l'origen introduït per l'usuari si es tracta d'unes coordenades o un nom d'una localització.
     * @param str és una coordenada o el nom d'una localització
     */
    private String validateOrgDest(String str) {
        boolean validLat = false;
        boolean validLon = false;
        if (str.contains(",")) {
            String[] coords = str.split("\\,");
            validLat = coordenadesValid(coords[0],"lat");
            validLon = coordenadesValid(coords[1], "lon");
            if (validLat && validLon) {
                return coords[0] + "," + coords[1];
            }
            return null;
        }
        Location location = getLocalitzacioExistent(str);
        if (location != null) {
            return location.getCoordinates()[1] + "," + location.getCoordinates()[0];
        }
        return null;
    }

    /**
     * Comprova que s'introdueix (s/a)
     * @param str string que indica si es de sortida o arribada
     * @return un false o un true per saber si es de sortida o arribada
     */
    private String validateSortidaArribada(String str) {
        if (str.length() > 1 || (str.charAt(0) != 's' && str.charAt(0) != 'a')) {
            System.out.println("Error! S'ha d'introduir 's' o 'a'!\n");
        }
        if (str.charAt(0) == 's') {
            return "false";
        }
        if (str.charAt(0) == 'a') {
            return "true";
        }
        return null;
    }
    /**
     * Converteix els segons en minuts
     * @param seg els segons a convertir
     * @return el valor dels segons en minuts arrodonits
     */
    private int conversorSegMin(int seg) {
        float aux = (float) seg / 60;
        int result = Math.round(seg / 60);
        if (aux >= 0.5 && aux <= 0.99) {
            result = 1;
        }
        return result;
    }

    /**
     * Comprova que la fecha introduïda segueixi el format i existeix en el calendari
     * @param str conté la data
     * @return un booleà
     */
    private boolean validarData (String str) {
        SimpleDateFormat pattern = new SimpleDateFormat("MM-dd-yyyy");
        pattern.setLenient(false);
        try {
            Date date = pattern.parse(str);
        }
        catch (ParseException e) {
            System.out.println("\nError, hi ha algun paràmetre erroni:(\n");
            return false;
        }
        return true;
    }

    /**
     * Mostra la ruta cap a la destinació que ha introduït l'usuari
     * @param plan dades tret de la api sobre la ruta
     */
    private void showPlanRuta(PlanResponse plan) {
        int walkDuration = 0;
        int h = 1;
        boolean flag = false;
        System.out.println("\tCombinació més ràpida: ");
        Itinerary itinerary = plan.getPlan().getItineraries()[1];
        int legSize = itinerary.getLegs().length - 1;
        System.out.println("\tTemps del trajecte: " + conversorSegMin(itinerary.getDuration()) + " min");
        System.out.println("\tOrigen");
        for (int i = 0; i < itinerary.getLegs().length; i++) {
            Leg leg = itinerary.getLegs()[i];
            //System.out.println("\t|");
            if (leg.getMode().equals("WALK")) {
                //System.out.println("\tcaminar " + conversorSegMin(leg.getDuration()) + " min");
                walkDuration += conversorSegMin(leg.getDuration());
                if (i < legSize) {
                    if (!itinerary.getLegs()[i+1].getMode().equals("WALK")) {
                        flag = true;
                    }
                }
                if (i == legSize) {
                    flag = true;
                }
            }
            if (flag) {
                System.out.println("\t|");
                System.out.println("\tcaminar " + walkDuration + " min");
                flag = false;
                walkDuration = 0;
            }
            if (leg.getMode().equals("SUBWAY") || leg.getMode().equals("BUS")) {
                System.out.println("\t|");
                System.out.print("\t" + leg.getRoute() + " " + leg.getFrom().getName() + "(" + leg.getFrom().getStopCode() + ")");
                if (leg.getStops() != null) {
                    for (int j = 0; j < leg.getStops().length; j++) {
                        System.out.print(" -> ");
                        System.out.print(leg.getStops()[j].getName() + "(" + leg.getStops()[j].getStopCode() + ")");
                    }
                }
                System.out.print("-> " + leg.getTo().getName() + "(" + leg.getTo().getStopCode() + ")");
                System.out.println(" " + conversorSegMin(leg.getDuration()) + " min");
            }
        }
        System.out.println("\t|");
        System.out.println("\tDestí");
    }

    /* ---------------------------------------- Cas 4: Temps d'espera del bus --------------------------------------- */

    /**
     * Mostrar el temps d'espera de bus en una parada
     */
    private void getTempsEsperaBus() {
        TMBClient tmb = new TMBClient();
        Gson gson = new Gson();
        Scanner sc = new Scanner(System.in);
        String jsonResponse = "";
        String codi;
        boolean okParada = false;
        do {
            System.out.println("\nIntrodueix el codi de parada:");
            codi = sc.nextLine();
            if (!isInteger (codi,10)) {
                System.out.println("\nError, codi de parada no vàlid!");
            } else {
                jsonResponse = tmb.getTempsEsperaBus(Integer.parseInt(codi));
                if(!jsonResponse.isEmpty()) {
                    iBusResponse tempsEspera = gson.fromJson(jsonResponse, iBusResponse.class);
                    okParada = printTempsEspera(tempsEspera);
                }
            }
        } while (!isInteger(codi, 10) || jsonResponse.isEmpty() || !okParada);

    }

    /**
     * Comprova si un string és numeric
     * @param str el string que es vol comprovar
     * @param rad el radix
     * @return true si és numeric. Altrament, false.
     */
    private static boolean isInteger(String str, int rad) {
        Scanner sc = new Scanner(str.trim());
        if (!sc.hasNextInt(rad))
            return false;
        sc.nextInt(rad);
        return !sc.hasNext();
    }

    /**
     * Mostrar el temps d'espera del bus per pantalla
     * @param tempsEspera la informació de temps d'espera dels busos a la parada
     * @return true si se ha pogut mostrar la informació correctament. Altrament, false.
     */
    private boolean printTempsEspera(iBusResponse tempsEspera) {
        System.out.println();
        int length = tempsEspera.getData().getIbus().length;
        if (length == 0) {
            System.out.println("Error, codi de parada no vàlid!");
            return false;
        } else {
            // Mostrar el temps d'espera per pantalla
            for (int i = 0; i < length; i++) {
                iBus bus = tempsEspera.getData().getIbus()[i];
                System.out.println(bus.getLine() + " - " + bus.getDestination() + " - " + bus.getTextCa());
            }
            return true;
        }
    }
}
