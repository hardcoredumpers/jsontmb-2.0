package webservices;

import model.Planner;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import java.io.IOException;

/**
 * Representa el client que gestiona totes les sol·licituds a la API de TMB.
 */
public class TMBClient {
    private static final String URL = "https://api.tmb.cat/v1/";
    private static final String APP_ID = "e0ab49de";
    private static final String APP_KEY = "976b3ce4df32d470f6d67a77d6e40984";
    private OkHttpClient client;

    public TMBClient() {
        client = new OkHttpClient();
    }

    /**
     * Agafa el temps d'espera del bus de la parada des de la API.
     * @param codiParada el codi de la parada del bus.
     * @return la resposta de la crida a la API de TMB.
     */
    public String getTempsEsperaBus (int codiParada) {
        String jsonResponse = "";
        String urlString = URL + "ibus/stops/" + codiParada + "?app_id=" + APP_ID + "&" + "app_key=" + APP_KEY;

        try {
            Request request = new Request.Builder()
                    .url(urlString)
                    .build();
            Response response = client.newCall(request).execute();

            int responseCode = response.code();
            if (responseCode != 200) {
                if (responseCode == 400) {
                    System.out.println("\nError, codi de parada no vàlid!");
                }
            } else {
                if (response.body() != null) {
                    jsonResponse = response.body().string();
                }
            }
        }
        catch (IOException e) {
            if(e.getMessage().contains("handshake_failure")) {
                System.out.println("TMBClient (getTempsEsperaBus) > " + e.getMessage());
            }
        }
        return jsonResponse;
    }

    /**
     * Agafa totes les línes de bus o metro des de la API
     * @param type el tipus de la linia. Pot ser bus o metro.
     * @return la resposta de la crida a la API de TMB.
     */
    public String getLinies (String type) {
        String jsonResponse = "";
        String urlString = URL + "transit/linies/" + type + "?app_id=" + APP_ID + "&" + "app_key=" + APP_KEY;

        try {
            Request request = new Request.Builder()
                    .url(urlString)
                    .build();
            Response response = client.newCall(request).execute();

            int responseCode = response.code();
            if (responseCode != 200) {
                throw new RuntimeException("ResponseCode: " + responseCode);
            } else {
                if (response.body() != null) {
                    jsonResponse = response.body().string();
                }
            }
        }
        catch (IOException e) {
            if(e.getMessage().contains("handshake_failure")) {
                System.out.println("TMBClient (getLinies) > " + e.getMessage());
            }
        }
        return jsonResponse;
    }

    /**
     * Agafa les estacions d'una linia de metro des de la API de TMB.
     * @param codi el codi de la linia.
     * @return la resposta de la crida a la API de TMB.
     */
    public String getLiniesEstacions(String codi) {
        String jsonResponse = "";
        String urlString = URL + "transit/linies/metro/" + codi + "/estacions?app_id=" + APP_ID + "&" + "app_key=" + APP_KEY;

        try {
            Request request = new Request.Builder()
                    .url(urlString)
                    .build();
            Response response = client.newCall(request).execute();

            int responseCode = response.code();
            if (responseCode != 200) {
                throw new RuntimeException("ResponseCode: " + responseCode);
            } else {
                if (response.body() != null) {
                    jsonResponse = response.body().string();
                }
            }
        }
        catch (IOException e) {
            if(e.getMessage().contains("handshake_failure")) {
                System.out.println("TMBClient (getLinies) > " + e.getMessage());
            }
        }
        return jsonResponse;
    }

    /**
     * Agafa totes les parades de bus de TMB.
     * @return la resposta de la crida a la API de TMB.
     */
    public String getParades () {
        String jsonResponse = "";
        String urlString = URL + "transit/parades?app_id=" + APP_ID + "&" + "app_key=" + APP_KEY;

        try {
            Request request = new Request.Builder()
                    .url(urlString)
                    .build();
            Response response = client.newCall(request).execute();

            int responseCode = response.code();
            if (responseCode != 200) {
                throw new RuntimeException("ResponseCode: " + responseCode);
            } else {
                if (response.body() != null) {
                    jsonResponse = response.body().string();
                }
            }
        }
        catch (IOException e) {
            if(e.getMessage().contains("handshake_failure")) {
                System.out.println("TMBClient (getParades) > " + e.getMessage());
            }
        }
        return jsonResponse;
    }

    /**
     * Agafa totes les estacions de metro de TMB.
     * @return la resposta de la crida a la API de TMB.
     */
    public String getEstacions() {
        String jsonResponse = "";
        String urlString = URL + "transit/estacions?app_id=" + APP_ID + "&" + "app_key=" + APP_KEY;

        try {
            Request request = new Request.Builder()
                    .url(urlString)
                    .build();
            Response response = client.newCall(request).execute();

            int responseCode = response.code();
            if (responseCode != 200) {
                throw new RuntimeException("ResponseCode: " + responseCode);
            } else {
                if (response.body() != null) {
                    jsonResponse = response.body().string();
                }
            }
        }
        catch (IOException e) {
            if(e.getMessage().contains("handshake_failure")) {
                System.out.println("TMBClient (getEstacions) > " + e.getMessage());
            }
        }
        return jsonResponse;
    }

    /**
     * Planifica una ruta a partir de la API de TMB.
     * @param plan
     * @return la resposta de la crida a la API de TMB.
     */
    public String planRuta (Planner plan) {
        String jsonResponse = "";
        String urlString = URL + "planner/plan?app_id=" + APP_ID + "&app_key=" + APP_KEY;
        String fromPlace = urlString + "&fromPlace=" + plan.getFromPlace();
        String toPlace = fromPlace + "&toPlace=" + plan.getToPlace();
        String date = toPlace + "&date=" + plan.getDate();
        String time = date + "&time=" + plan.getTime();
        String arriveBy = time + "&arriveBy=" + plan.getArriveBy();
        String finalUrl = arriveBy + "&mode=TRANSIT,WALK&maxWalkDistance=" + plan.getMaxWalkDistance();
        try {
            Request request = new Request.Builder()
                    .url(finalUrl)
                    .build();
            Response response = client.newCall(request).execute();

            int responseCode = response.code();
            if (responseCode != 200) {
                //throw new RuntimeException("ResponseCode: " + responseCode);
                System.out.println("TMB està fent tot el possible perquè el bus i el metro facin aquesta ruta en un futur.\n");
            } else {
                if (response.body() != null) {
                    jsonResponse = response.body().string();
                }
            }
        }
        catch (IOException e) {
            if(e.getMessage().contains("handshake_failure")) {
                System.out.println("TMBClient (planRuta) > " + e.getMessage());
            }
        }
        return jsonResponse;
    }
}
